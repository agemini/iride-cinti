$(function () {
    //
    // Scroll Reveal Settings and Init
    //
    $('.slide').each(function (index, el) {
        $(el).find('.anim-block').each(function (i, animBlock) {
            $(animBlock).wrap('<div data-sr="ease-out move 150px over 1s wait ' + parseFloat(i * 0.25) + 's"></div>');
        });
    });

    //
    // Lazy Image Loading Configuration and
    //
    $("img[data-original]").lazyload({
        //	threshold: $(window).height() * 2,
        effect : "fadeIn"
    });
});

// Window Load
$(window).load(function () {

    // Default Easing
    var default_ease = Sine.easeInOut; //Power1.easeInOut;

    // Object (.anim-block) asynchronous concurrent animations
    var tweens = [
        { 	// X axis delta shift
        	from: {
        		x: -20,
        		ease: default_ease,
        		transformOrigin: 'center'
        	},
        	to: {
        		x: 20,
        		ease: default_ease,
        		transformOrigin: 'center'
        	}
        },
        {	// Resize delta shift
        	from: {
        		scale: 0.99,
        		ease: default_ease,
        		transformOrigin: 'center'
        	},
        	to: {
        		scale: 1.01,
        		ease: default_ease,
        		transformOrigin: 'center'
        	}
        },
        {	// Y axis delta shift
        	from: {
        		y: -50,
        		ease: default_ease,
        		transformOrigin: 'center'
        	},
        	to: {
        		y: 50,
        		ease: default_ease,
        		transformOrigin: 'center'
        	}
        },
        {	// Rotate X delta
            from: {
        		rotate: -0.01,
        		ease: default_ease,
        		transformOrigin: 'center'	
        	},
        	to: {
        		rotate: 0.01,
        		ease: default_ease,
        		transformOrigin: 'center'
        	}
        }
    ];

    // All animations timeline
    var animations_timeline = new TimelineMax({repeat: -1}); // -1 = infinite

    $('.anim-block').each(function (i, e) {
        // Apply animation tweens to each object
        $(tweens).each(function (index, tween) {

            var duration_range = [3, 7], // <- Change single object animation ( [min, max]) in seconds

            duration = duration_range[0] * Math.random() + (duration_range[1] - duration_range[0]),
            element_timeline = new TimelineMax({
                repeat: -1,
                yoyo: true,
                delay: 0.05 * i,
                startTime: parseInt(Math.random() * duration)
            });



            element_timeline.insert(
                new TweenMax($(e),
                    duration,
                    tween.from,
                    tween.to,
                    { ease: tween.default_ease, progress: parseInt(0.05 * i, 10), overwrite: "all"}
                )
            );
            animations_timeline.insert(element_timeline);
        });

    });

    startAutoScroll();

    onLoadInit();

})

// Windows Autoscroll
var autoscroll_fps = 90,
	last_known_scrollY = 0;

$(window).on('scroll', function(event) {
	if (last_known_scrollY - window.scrollY > 5 || window.scrollY - last_known_scrollY > 5) {
		autoscroll_fps = 0;
	}

	last_known_scrollY = window.scrollY;
});

setInterval(function(){
	if (autoscroll_fps < 90) {
		autoscroll_fps++;
	}
}, autoscroll_fps * 4);

function autoscroll() {
   window.autoscroll_timeout = setTimeout(function () {
       requestAnimationFrame(autoscroll);

       window.scrollTo(
           window.scrollX,
           window.scrollY + 1 // Autoscroll Speed Mulitplier
       );
   }, 1000 / autoscroll_fps);
}

function startAutoScroll(){
	window.autoscroll_timeout = setTimeout(function () {
       autoscroll();
    }, 1000);
}

window.onbeforeunload = function(){
	document.getElementById('main-content').classList.remove('loaded');
	window.scrollTo(0,0);
}

function onLoadInit(){
	// Reset window scroll position
	window.scrollTo(0,0);
	$(window).scrollTop(0);
	last_known_scrollY = 0;
	$('video').each(function(index, el) {
		el.play()
	});

	// Set main-content loaded
	$('#main-content, body').addClass('loaded');

	// Init ScrollReveal
	window.sr = new scrollReveal({
        scale: {direction: 'up', power: '5%'},
        vFactor: 0.5,
        reset: false
    });

    $('.modal').on('show.bs.modal', function(event) {
		//clearTimeout(window.autoscroll_timeout);
		$('html,body').animate({
			scrollTop: $(event.relatedTarget).offset().top - 20,
		}, 1000, 'swing', function(){
			clearTimeout(window.autoscroll_timeout);	
		});
	}).on('shown.bs.modal', function(event) {
		clearTimeout(window.autoscroll_timeout);	
	}).on('hide.bs.modal', function(event) {
		startAutoScroll();
	});
}

/* ANIMAZIONI GEOMETRICHE*/

function rotateShape(id)
{
    var shape = document.getElementById(id);
    TweenMax.to(shape, 2, {rotate: 3, repeat: -1, yoyo: true, ease: Power1.easeInOut});
}

function rotateShapeLoop(id)
{
    var shape = document.getElementById(id);
    TweenMax.to(shape, 25, {rotate: 360, ease:Linear.easeNone, repeat: -1 });       
}

function zoomInOut(){
    TweenMax.to(document.getElementById('id'), 2, {scale: 0, repeat: -1, yoyo: true, ease: Power1.easeInOut });    
}

function fades()
{
    TweenMax.to(document.getElementById("bordo-stivale"), 2.5, {opacity: 0, repeat: -1, yoyo: true, ease: Power1.easeInOut});
    TweenMax.to(document.getElementById("triangolo-fill"), 2.5, {opacity: 0, repeat: -1, yoyo: true, ease: Power1.easeInOut});
    TweenMax.to(document.getElementById("quadrato-s"), 3, {opacity: 0, repeat: -1, yoyo: true, ease: Power1.easeInOut});
    TweenMax.to(document.getElementById("cerchio-s"), 3, {opacity: 0, repeat: -1, yoyo: true, ease: Power1.easeInOut, delay: 1});
    TweenMax.to(document.getElementById("triangolo-lampeggiante"), 3, {opacity: 0, repeat: -1, yoyo: true, ease: Power1.easeInOut, delay: 1});
}

$().ready(function() {
    rotateShape("triangolo");
    rotateShapeLoop("triangolo-stivale");
    //zoomInOut();
    fades();
});